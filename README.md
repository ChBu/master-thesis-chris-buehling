# Appendix C: Results of runs

The results gathered by each respective run, as described in chapter 4, and evaluated in chapter 5 of the master's thesis can be accessed publicly via this gitlab repository.

The repository contains:

•	All runs described in the thesis, divided into .zip-directories by optimization algorithms and run. Each such directory contains:
-	the job file,
-	the start file,
-	the out and job.out files,
-	the /model/log directory, and
-	the /python folder

 of each run.

•	Appendices A and B as .pdf files.
